//
//  HelloWorld.swift
//  Lesson4
//
//  Created by Polina on 10/5/17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

class HelloWorld: NSObject {

    /*
     1. написати метод що виводить привітання “hello word”
     */
    
    /*
    static func displayGreeting() {
        print("Hello world!")
    }
   */
    
    /*
     2. написать метод который будет выводить приветствие
     передаваемое аргументом количество раз
     */
    
    /*
    static func displayGreeting(countOfGreetings: Int) {
        for _ in 0..<countOfGreetings {
            print("Hello world!")
        }
    }
     */
 
    /*
    3. написать метод в который передается номер дня(1-7),
     и выводится строка с названием дня недели
     */
    
    /*
    static func daysOfTheWeek(number: Int) {
        switch number {
        case 1:
            print("Today is Monday.")
            break
        case 2:
            print("Today is Tuesday.")
            break
        case 3:
            print("Today is Wednesday.")
            break
        case 4:
            print("Today is Thursday.")
            break
        case 5:
            print("Today is Friday.")
            break
        case 6:
            print("Today is Saturday.")
            break
        default:
            print("Today is Sunday.")
        }
    }
    */
    
    /*
     4. сделать метод 3 не зависимым от номера дня
     (8- понедельник, 9 - вторник и т д)
     */
    
    /*
    static func daysOfTheWeekEpanded(number: Int) {
        switch number%7 {
        case 1:
            print("Today is Monday.")
            break
        case 2:
            print("Today is Tuesday.")
            break
        case 3:
            print("Today is Wednesday.")
            break
        case 4:
            print("Today is Thursday.")
            break
        case 5:
            print("Today is Friday.")
            break
        case 6:
            print("Today is Saturday.")
            break
        default:
            print("Today is Sunday.")
        }
    }
     */
    
    /*
     5. Написать метод который выводит числа Фибоначчи до F(n)
     */
    
    /*
    static func fibbonachiNumbers(number: Int) {
        var previousNum = 0
        var currentNum = 1
        for _ in 2..<number {
            previousNum = currentNum
            currentNum += previousNum
        }
        print("The \(number) number is \(currentNum)")
    }
    */
    
    /*
     6. Дюймы в сантиметры 1 дюйм = 2.54см
     */

    /*
    static func converter(length: Int) {
        var inch = 2.54
        var smLength: Double = 0
        smLength = Double(length) * inch
        print("\(length) inches in sm is \(smLength)")
    }
    */
    
    /*
     7. Сколько можно петь эту песню: 99 bottles of beer on the wall. 99 bottles of beer. You take one down, pass it around. 98 bottles of beer on the wall.  98 bottles of beer on the wall. . . .
     */

    /*
    static func song(countOfBottles: Int) {
        for bottles in (1..<countOfBottles).reversed() {
            print("\(bottles) botteles of beer on the wall.")
            print("\(bottles) bottles of beer")
            print("You can take one down, pass it around. \(bottles - 1) bottles of beer on the wall")
            print("\(bottles - 1) bottles of beer")
        }
    }
    */
    
    /*
     8. Написать программу которая выводит все числа которые
     можно нацело делить введенным числом в рамках 1…100
     */
    
    static func dividerNumbers(divided: Int) {
        for index in (1...100).reversed() {
            if index%divided == 0 {
                print("\(index) divide on \(divided) without mod")
            }
        }
    }
}
