//
//  ViewController.swift
//  Lesson4
//
//  Created by Polina on 05.10.17.
//  Copyright © 2017 Polina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let number = Double(inputTextField.text!)!
        print(number)
        let intNum = Int(number)
        
        //HelloWorld.displayGreeting()
        //HelloWorld.displayGreeting(countOfGreetings: intNum)
        //HelloWorld.daysOfTheWeek(number: intNum)
        //HelloWorld.daysOfTheWeekEpanded(number: intNum)
        //HelloWorld.fibbonachiNumbers(number: intNum)
        //HelloWorld.converter(length: intNum)
        //HelloWorld.song(countOfBottles: intNum)
        HelloWorld.dividerNumbers(divided: intNum)
    }

}


